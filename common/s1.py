# -*- coding: utf-8 -*-
from lxml import etree
import os
import logging

logger = logging.getLogger('s1')


def zip_extract(**kwargs):
    import zipfile

    path = os.path.dirname(kwargs['src_file'])

    if not os.path.exists(path):
        os.makedirs(path)

    result = False
    zip = zipfile.ZipFile(kwargs['src_file'])

    try:
        zip.extractall(kwargs['dst_path'])

        result = True
    except BaseException, e:
        logger.debug(e)
    finally:
        zip.close()

    return result


def _group_parse(node, **kwargs):
    if node is None:
        return

    kwargs['level'] += 1

    for group in node.iterfind(u'Группа'):
        result = {}

        for attr_node in group:
            result[attr_node.tag] = attr_node.text

        if 'parent' in kwargs:
            result['parent'] = kwargs['parent']
        result['level'] = kwargs['level']

        kwargs['handlers']['group'](result)

        kwargs['parent'] = result

        if not _group_parse(group.find(u'Группы'), **kwargs):
            break


def _prop_parse(node, **kwargs):
    if node is None:
        return

    for prop in node:
        result = {}

        for attr_node in prop:
            if len(attr_node):
                sub_attrs = []
                for sub_attr_node in attr_node:
                    sub_attrs.append({
                        sub_attr_node.tag: sub_attr_node.text
                    })
                result[attr_node.tag] = sub_attrs
            else:
                result[attr_node.tag] = attr_node.text

        if not kwargs['handler'](result):
            break


def _product_parse(node, **kwargs):
    if node is None:
        return

    sub_prop = {
        u'Комплектующие': 1,
        u'Модели': 1,
        u'Партномера': 1,
        u'Аналоги': 1,
        u'ЗначенияСвойств': 1,
        u'СтавкиНалогов': 1,
        u'ЗначенияРеквизитов': 1,
    }

    for prop in node:
        result = {}

        for attr_node in prop:
            if attr_node.tag in sub_prop:
                result[attr_node.tag] = {}

                def handler(sub_result):
                    if u'Ид' in sub_result:
                        result[attr_node.tag][sub_result[u'Ид']] = sub_result
                    else:
                        result[attr_node.tag][len(result[attr_node.tag])] = sub_result

                    return True

                _prop_parse(attr_node, handler=handler)

                continue

            if len(attr_node):
                sub_attrs = []
                for i, sub_attr_node in enumerate(attr_node):
                    sub_attrs.append({
                        sub_attr_node.tag: sub_attr_node.text
                    })
                result[attr_node.tag] = sub_attrs
            else:
                result[attr_node.tag] = attr_node.text

        if not kwargs['handler'](result, **kwargs):
            break


def fs_import_parse(**kwargs):
    logger.debug('Parse start')
    prefix = kwargs.get('dst_prefix', '1cbitrix')
    tree = etree.parse('%s/%s/import.xml' % (kwargs['dst_path'], prefix))
    handlers = kwargs.get('handlers', {})

    kwargs['is_full'] = tree.find(u'Каталог').attrib[u'СодержитТолькоИзменения'] == 'false'


    if 'group' in handlers:
        groups = tree.find(u'Классификатор/Группы')
        kwargs['level'] = 0
        logger.debug('group parse start')
        _group_parse(groups, **kwargs)

    if 'prop' in handlers:
        props = tree.find(u'Классификатор/Свойства')
        kwargs['prop_name'] = u'Свойство'
        kwargs['handler'] = handlers['prop']
        logger.debug('classif parse start')
        _prop_parse(props, **kwargs)

    if 'brand' in handlers:
        props = tree.find(u'Классификатор/Бренды')
        kwargs['prop_name'] = u'Бренд'
        kwargs['handler'] = handlers['brand']

        _prop_parse(props, **kwargs)

    # after brand, because has brand FK
    if 'model' in handlers:
        props = tree.find(u'Классификатор/Модели')
        kwargs['prop_name'] = u'Модель'
        kwargs['handler'] = handlers['model']

        _prop_parse(props, **kwargs)

    # after brand, because has brand FK
    if 'part_number' in handlers:
        props = tree.find(u'Классификатор/Партномера')
        kwargs['prop_name'] = u'Партномер'
        kwargs['handler'] = handlers['part_number']

        _prop_parse(props, **kwargs)

    if 'product' in handlers:
        logger.debug('product parse start')
        props = tree.find(u'Каталог/Товары')
        # kwargs['prop_name'] = u'Товар'
        kwargs['handler'] = handlers['product']
        _product_parse(props, **kwargs)